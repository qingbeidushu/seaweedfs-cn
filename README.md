<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">海藻FS</font></font></h1><a id="user-content-seaweedfs" class="anchor" aria-label="永久链接：SeaweedFS" href="#seaweedfs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://join.slack.com/t/seaweedfs/shared_invite/enQtMzI4MTMwMjU2MzA3LTEyYzZmZWYzOGQ3MDJlZWMzYmI0OTE4OTJiZjJjODBmMzUxNmYwODg0YjY3MTNlMjBmZDQ1NzQ5NDJhZWI2ZmY" rel="nofollow"><img src="https://camo.githubusercontent.com/59b517756cdb49f0b1eaaf79b6983b382ab67cb903417462ecdbf43ecb2ac052/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f736c61636b2d707572706c65" alt="松弛" data-canonical-src="https://img.shields.io/badge/slack-purple" style="max-width: 100%;"></a>
<a href="https://twitter.com/intent/follow?screen_name=seaweedfs" rel="nofollow"><img src="https://camo.githubusercontent.com/a083358e8946de36f2f8ab78aa6a11b40b0231f207f2f21bac22176965e9e9ec/68747470733a2f2f696d672e736869656c64732e696f2f747769747465722f666f6c6c6f772f7365617765656466732e7376673f7374796c653d736f6369616c266c6162656c3d466f6c6c6f77" alt="推特" data-canonical-src="https://img.shields.io/twitter/follow/seaweedfs.svg?style=social&amp;label=Follow" style="max-width: 100%;"></a>
<a href="https://github.com/seaweedfs/seaweedfs/actions/workflows/go.yml"><img src="https://camo.githubusercontent.com/b5bc1f91202f616e89019507e3885e21fb004133de2d3d80bd25e6355593f1b9/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f616374696f6e732f776f726b666c6f772f7374617475732f7365617765656466732f7365617765656466732f676f2e796d6c" alt="构建状态" data-canonical-src="https://img.shields.io/github/actions/workflow/status/seaweedfs/seaweedfs/go.yml" style="max-width: 100%;"></a>
<a href="https://godoc.org/github.com/seaweedfs/seaweedfs/weed" rel="nofollow"><img src="https://camo.githubusercontent.com/636bf807691a4ed20d439409a16f5b928cbdc04313bcec5b10a36f7905d6c676/68747470733a2f2f676f646f632e6f72672f6769746875622e636f6d2f7365617765656466732f7365617765656466732f776565643f7374617475732e737667" alt="戈多克" data-canonical-src="https://godoc.org/github.com/seaweedfs/seaweedfs/weed?status.svg" style="max-width: 100%;"></a>
<a href="https://github.com/seaweedfs/seaweedfs/wiki"><img src="https://camo.githubusercontent.com/298914534150f5a23c13ca9bb5bf3f55b9f08eab3416cf04681eadc61cba7bb8/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f646f63732d77696b692d626c75652e737667" alt="维基百科" data-canonical-src="https://img.shields.io/badge/docs-wiki-blue.svg" style="max-width: 100%;"></a>
<a href="https://hub.docker.com/r/chrislusf/seaweedfs/" rel="nofollow"><img src="https://camo.githubusercontent.com/7cd8973dd597aee90df6d3e88c0b87d33444b595d9bd277f83f47793486e525f/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f63687269736c7573662f7365617765656466733f6d61784167653d34383030" alt="Docker 拉取" data-canonical-src="https://img.shields.io/docker/pulls/chrislusf/seaweedfs?maxAge=4800" style="max-width: 100%;"></a>
<a href="https://search.maven.org/search?q=g:com.github.chrislusf" rel="nofollow"><img src="https://camo.githubusercontent.com/ae85de47e54a7187520e8e6b3f672fb8073ff046dc3da29726d1c08381c019bc/68747470733a2f2f696d672e736869656c64732e696f2f6d6176656e2d63656e7472616c2f762f636f6d2e6769746875622e63687269736c7573662f7365617765656466732d636c69656e74" alt="Maven Central 上的 SeaweedFS" data-canonical-src="https://img.shields.io/maven-central/v/com.github.chrislusf/seaweedfs-client" style="max-width: 100%;"></a>
<a href="https://artifacthub.io/packages/search?repo=seaweedfs" rel="nofollow"><img src="https://camo.githubusercontent.com/80b9f230431dd4087c0b185037652d5cba886a7b1ab901ab41a8eb42af87e9ea/68747470733a2f2f696d672e736869656c64732e696f2f656e64706f696e743f75726c3d68747470733a2f2f61727469666163746875622e696f2f62616467652f7265706f7369746f72792f736561776565646673" alt="神器中心" data-canonical-src="https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/seaweedfs" style="max-width: 100%;"></a></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/seaweedfs/seaweedfs/master/note/seaweedfs.png"><img src="https://raw.githubusercontent.com/seaweedfs/seaweedfs/master/note/seaweedfs.png" alt="SeaweedFS 标志" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 align="center" tabindex="-1" class="heading-element" dir="auto"><a href="https://www.patreon.com/seaweedfs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 Patreon 赞助 SeaweedFS</font></font></a></h2><a id="user-content-sponsor-seaweedfs-via-patreon" class="anchor" aria-label="永久链接：通过 Patreon 赞助 SeaweedFS" href="#sponsor-seaweedfs-via-patreon"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 是一个独立的 Apache 许可的开源项目，其持续开发的实现完全归功于这些出色的</font></font><a href="https://github.com/seaweedfs/seaweedfs/blob/master/backers.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持者</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的支持。如果您想让 SeaweedFS 变得更加强大，请考虑加入我们的
</font></font><a href="https://www.patreon.com/seaweedfs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Patreon 赞助商</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我和其他支持者将非常感谢您的支持！</font></font></p>

<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金牌赞助商</font></font></h3><a id="user-content-gold-sponsors" class="anchor" aria-label="永久链接：金牌赞助商" href="#gold-sponsors"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://www.nodion.com" rel="nofollow"><img src="https://raw.githubusercontent.com/seaweedfs/seaweedfs/master/note/sponsor_nodion.png" alt="节点" style="max-width: 100%;"></a>
<a href="https://www.piknik.com" rel="nofollow"><img src="https://raw.githubusercontent.com/seaweedfs/seaweedfs/master/note/piknik.png" alt="皮克尼克" style="max-width: 100%;"></a></p>
<hr>
<ul dir="auto">
<li><a href="https://github.com/seaweedfs/seaweedfs/releases/latest"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载不同平台的二进制文件</font></font></a></li>
<li><a href="https://join.slack.com/t/seaweedfs/shared_invite/enQtMzI4MTMwMjU2MzA3LTEyYzZmZWYzOGQ3MDJlZWMzYmI0OTE4OTJiZjJjODBmMzUxNmYwODg0YjY3MTNlMjBmZDQ1NzQ5NDJhZWI2ZmY" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Slack 上的 SeaweedFS</font></font></a></li>
<li><a href="https://twitter.com/SeaweedFS" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Twitter 上的 SeaweedFS</font></font></a></li>
<li><a href="https://t.me/Seaweedfs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Telegram 上的 SeaweedFS</font></font></a></li>
<li><a href="https://www.reddit.com/r/SeaweedFS/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reddit 上的 SeaweedFS</font></font></a></li>
<li><a href="https://groups.google.com/d/forum/seaweedfs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 邮件列表</font></font></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维基文档</font></font></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/SeaweedFS_Architecture.pdf"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 白皮书</font></font></a></li>
<li><a href="https://docs.google.com/presentation/d/1DcxKWlINc-HNCjhYeERkpGXXm6nTCES8mi2W5G0Z4Ts/edit?usp=sharing" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 介绍幻灯片 2021.5</font></font></a></li>
<li><a href="https://www.slideshare.net/chrislusf/seaweedfs-introduction" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 介绍幻灯片 2019.3</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></h1><a id="user-content-table-of-contents" class="anchor" aria-label="固定链接：目录" href="#table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="#quick-start"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速开始</font></font></a>
<ul dir="auto">
<li><a href="#quick-start-for-s3-api-on-docker"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 上的 S3 API 快速入门</font></font></a></li>
<li><a href="#quick-start-with-single-binary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用单个二进制文件快速启动</font></font></a></li>
<li><a href="#quick-start-seaweedfs-s3-on-aws"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AWS 上的快速入门 SeaweedFS S3</font></font></a></li>
</ul>
</li>
<li><a href="#introduction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">介绍</font></font></a></li>
<li><a href="#features"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></a>
<ul dir="auto">
<li><a href="#additional-features"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">附加功能</font></font></a></li>
<li><a href="#filer-features"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">归档功能</font></font></a></li>
</ul>
</li>
<li><a href="#example-using-seaweed-object-store"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">示例：使用 Seaweed 对象存储</font></font></a></li>
<li><a href="#object-store-architecture"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建筑学</font></font></a></li>
<li><a href="#compared-to-other-file-systems"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与其他文件系统相比</font></font></a>
<ul dir="auto">
<li><a href="#compared-to-hdfs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与HDFS相比</font></font></a></li>
<li><a href="#compared-to-glusterfs-ceph"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 GlusterFS、Ceph 相比</font></font></a></li>
<li><a href="#compared-to-glusterfs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 GlusterFS 相比</font></font></a></li>
<li><a href="#compared-to-ceph"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 Ceph 相比</font></font></a></li>
<li><a href="#compared-to-minio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与米尼奥相比</font></font></a></li>
</ul>
</li>
<li><a href="#dev-plan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发计划</font></font></a></li>
<li><a href="#installation-guide"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装指南</font></font></a></li>
<li><a href="#disk-related-topics"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">磁盘相关主题</font></font></a></li>
<li><a href="#benchmark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准</font></font></a></li>
<li><a href="#license"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速开始</font></font></h1><a id="user-content-quick-start" class="anchor" aria-label="永久链接：快速入门" href="#quick-start"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 上的 S3 API 快速入门</font></font></h2><a id="user-content-quick-start-for-s3-api-on-docker" class="anchor" aria-label="永久链接：Docker 上的 S3 API 快速入门" href="#quick-start-for-s3-api-on-docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><code>docker run -p 8333:8333 chrislusf/seaweedfs server -s3</code></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用单个二进制文件快速启动</font></font></h2><a id="user-content-quick-start-with-single-binary" class="anchor" aria-label="永久链接：使用单个二进制文件快速入门" href="#quick-start-with-single-binary"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"></font><a href="https://github.com/seaweedfs/seaweedfs/releases"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从https://github.com/seaweedfs/seaweedfs/releases</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载最新的二进制文件</font><font style="vertical-align: inherit;">并解压缩单个二进制文件</font></font><code>weed</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><code>weed.exe</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.或者跑</font></font><code>go install github.com/seaweedfs/seaweedfs/weed@latest</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行</font></font><code>weed server -dir=/some/data/dir -s3</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以启动一台主服务器、一台卷服务器、一台文件管理器和一台 S3 网关。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此外，要增加容量，只需通过</font></font><code>weed volume -dir="/some/data/dir2" -mserver="&lt;master_host&gt;:9333" -port=8081</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在本地运行、或在不同的计算机上或在数千台计算机上运行来添加更多卷服务器即可。这就对了！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AWS 上的快速入门 SeaweedFS S3</font></font></h2><a id="user-content-quick-start-seaweedfs-s3-on-aws" class="anchor" aria-label="永久链接：在 AWS 上快速入门 SeaweedFS S3" href="#quick-start-seaweedfs-s3-on-aws"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><a href="https://aws.amazon.com/marketplace/pp/prodview-nzelz5gprlrjc" rel="nofollow"><font style="vertical-align: inherit;">使用 cloudformation 在 AWS 上</font></a><font style="vertical-align: inherit;">设置快速生产就绪的SeaweedFS S3</font></font><a href="https://aws.amazon.com/marketplace/pp/prodview-nzelz5gprlrjc" rel="nofollow"><font style="vertical-align: inherit;"></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">介绍</font></font></h1><a id="user-content-introduction" class="anchor" aria-label="永久链接：简介" href="#introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 是一个简单且高度可扩展的分布式文件系统。有两个目标：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储数十亿个文件！</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速提供文件！</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 最初是作为一个对象存储来有效处理小文件的。中央主机不管理中央主机中的所有文件元数据，而是仅管理卷服务器上的卷，而这些卷服务器管理文件及其元数据。这减轻了中央主服务器的并发压力，并将文件元数据分散到卷服务器中，从而允许更快的文件访问（O(1)，通常只需一次磁盘读取操作）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个文件的元数据仅需要 40 字节的磁盘存储开销。 O(1) 磁盘读取非常简单，欢迎您用实际用例来挑战性能。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 首先实现了</font></font><a href="http://www.usenix.org/event/osdi10/tech/full_papers/Beaver.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Facebook 的 Haystack 设计论文</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。此外，SeaweedFS 还借鉴了
</font></font><a href="https://www.usenix.org/system/files/conference/osdi14/osdi14-paper-muralidhar.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">f4（Facebook 的 Warm BLOB 存储系统）的思想实现了纠删码，与</font></font></a><font style="vertical-align: inherit;"><a href="https://www.usenix.org/system/files/fast21-pan.pdf" rel="nofollow"><font style="vertical-align: inherit;">Facebook 的 Tectonic Filesystem</font></a><font style="vertical-align: inherit;">有很多相似之处</font></font><a href="https://www.usenix.org/system/files/fast21-pan.pdf" rel="nofollow"><font style="vertical-align: inherit;"></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在对象存储之上，可选的</font></font><a href="https://github.com/seaweedfs/seaweedfs/wiki/Directories-and-Files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Filer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可以支持目录和 POSIX 属性。 Filer 是一个独立的可线性扩展的无状态服务器，具有可定制的元数据存储，例如 MySql、Postgres、Redis、Cassandra、HBase、Mongodb、Elastic Search、LevelDB、RocksDB、Sqlite、MemSql、TiDB、Etcd、CockroachDB、YDB 等。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于任何分布式键值存储，可以将大值卸载到 SeaweedFS。 SeaweedFS 具有快速的访问速度和线性可扩展的容量，可以作为分布式</font></font><a href="https://github.com/seaweedfs/seaweedfs/wiki/Filer-as-a-Key-Large-Value-Store"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Key-Large-Value 存储</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 可以透明地与云集成。热数据位于本地集群，温数据位于云上，访问时间为 O(1)，SeaweedFS 可以实现快速的本地访问时间和弹性的云存储容量。更重要的是，云存储访问API成本被最小化。比直接云存储更快、更便宜！</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h1><a id="user-content-features" class="anchor" aria-label="永久链接：特点" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">附加功能</font></font></h2><a id="user-content-additional-features" class="anchor" aria-label="永久链接：附加功能" href="#additional-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可以选择不复制或不同的复制级别、机架和数据中心感知。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动主服务器故障转移 - 无单点故障 (SPOF)。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据文件 MIME 类型自动进行 Gzip 压缩。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">删除或更新后自动压缩以回收磁盘空间。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Store-file-with-a-Time-To-Live"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动进入TTL过期</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任何具有一定磁盘空间的服务器都可以添加到总存储空间中。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">添加/删除服务器不会</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">导致</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任何数据重新平衡，除非由管理命令触发。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可选的图片大小调整。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 ETag、Accept-Range、Last-Modified 等</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持内存/leveldb/readonly模式调优，实现内存/性能平衡。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持重新平衡可写和只读卷。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Tiered-Storage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可定制的多个存储层</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：可定制的存储磁盘类型以平衡性能和成本。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Cloud-Tier"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">透明的云集成</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：通过分层云存储提供无限容量的温数据。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Erasure-coding-for-warm-storage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于热存储的纠删码</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">  Rack-Aware 10.4 纠删码可降低存储成本并提高可用性。</font></font></li>
</ul>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">归档功能</font></font></h2><a id="user-content-filer-features" class="anchor" aria-label="永久链接：文件管理器功能" href="#filer-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Directories-and-Files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件管理器服务器</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 HTTP 提供“普通”目录和文件。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Filer-Stores"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件 TTL</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动使文件元数据和实际文件数据过期。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/FUSE-Mount"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mount filer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 FUSE 直接将文件读写为本地目录。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Filer-Store-Replication"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件管理器存储复制</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为文件管理器元数据存储启用 HA。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Filer-Active-Active-cross-cluster-continuous-synchronization"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">双活复制</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持异步单向或双向跨集群连续复制。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Amazon-S3-API"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Amazon S3 兼容 API</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 S3 工具访问文件。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Hadoop-Compatible-File-System"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hadoop 兼容文件系统</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 Hadoop/Spark/Flink/etc 访问文件，甚至运行 HBase。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Async-Replication-to-Cloud"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">异步复制到云</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有极快的本地访问速度以及对 Amazon S3、Google Cloud Storage、Azure、BackBlaze 的备份。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/WebDAV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebDAV</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Mac 和 Windows 上或从移动设备上作为映射驱动器进行访问。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Filer-Data-Encryption"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AES256-GCM 加密存储</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全地存储加密数据。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Data-Structure-for-Large-Files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超大文件</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储数十TB的大型或超大文件。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Cloud-Drive-Architecture"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云硬盘</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将云存储安装到本地集群，并进行缓存以实现快速读写和异步回写。</font></font></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Gateway-to-Remote-Object-Storage"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><a href="https://github.com/seaweedfs/seaweedfs/wiki/Cloud-Drive-Architecture"><font style="vertical-align: inherit;">除了Cloud Drive之外，</font></a><a href="https://github.com/seaweedfs/seaweedfs/wiki/Gateway-to-Remote-Object-Storage"><font style="vertical-align: inherit;">远程对象存储网关</font></a><font style="vertical-align: inherit;">还将存储桶操作镜像到远程对象存储</font></font><a href="https://github.com/seaweedfs/seaweedfs/wiki/Cloud-Drive-Architecture"><font style="vertical-align: inherit;"></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">库伯内斯</font></font></h2><a id="user-content-kubernetes" class="anchor" aria-label="永久链接：Kubernetes" href="#kubernetes"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/seaweedfs/seaweedfs-csi-driver"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kubernetes CSI 驱动程序</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">容器存储接口 (CSI) 驱动程序。</font></font><a href="https://hub.docker.com/r/chrislusf/seaweedfs-csi-driver/" rel="nofollow"><img src="https://camo.githubusercontent.com/0ffe7815e3e0ef6c7109333e63c47874b86a6305535307c8cb7521b1c4bf5a52/68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f63687269736c7573662f7365617765656466732d6373692d6472697665722e7376673f6d61784167653d34383030" alt="Docker 拉取" data-canonical-src="https://img.shields.io/docker/pulls/chrislusf/seaweedfs-csi-driver.svg?maxAge=4800" style="max-width: 100%;"></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs-operator"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 操作员</font></font></a></li>
</ul>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">示例：使用 Seaweed 对象存储</font></font></h2><a id="user-content-example-using-seaweed-object-store" class="anchor" aria-label="永久链接：示例：使用 Seaweed 对象存储" href="#example-using-seaweed-object-store"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">默认情况下，主节点运行在端口 9333 上，卷节点运行在端口 8080 上。让我们启动一个主节点，在端口 8080 和 8081 上启动两个卷节点。理想情况下，它们应该从不同的机器启动。我们将使用 localhost 作为示例。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 使用 HTTP REST 操作来读取、写入和删除。响应采用 JSON 或 JSONP 格式。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启动主服务器</font></font></h3><a id="user-content-start-master-server" class="anchor" aria-label="永久链接：启动主服务器" href="#start-master-server"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; ./weed master
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> ./weed master" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">启动卷服务器</font></font></h3><a id="user-content-start-volume-servers" class="anchor" aria-label="永久链接：启动卷服务器" href="#start-volume-servers"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; weed volume -dir="/tmp/data1" -max=5  -mserver="localhost:9333" -port=8080 &amp;
&gt; weed volume -dir="/tmp/data2" -max=10 -mserver="localhost:9333" -port=8081 &amp;
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> weed volume -dir=&quot;/tmp/data1&quot; -max=5  -mserver=&quot;localhost:9333&quot; -port=8080 &amp;
> weed volume -dir=&quot;/tmp/data2&quot; -max=10 -mserver=&quot;localhost:9333&quot; -port=8081 &amp;" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">写入文件</font></font></h3><a id="user-content-write-file" class="anchor" aria-label="永久链接：写入文件" href="#write-file"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要上传文件：首先，发送 HTTP POST、PUT 或 GET 请求以</font></font><code>/dir/assign</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取</font></font><code>fid</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">卷服务器 URL：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; curl http://localhost:9333/dir/assign
{"count":1,"fid":"3,01637037d6","url":"127.0.0.1:8080","publicUrl":"localhost:8080"}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> curl http://localhost:9333/dir/assign
{&quot;count&quot;:1,&quot;fid&quot;:&quot;3,01637037d6&quot;,&quot;url&quot;:&quot;127.0.0.1:8080&quot;,&quot;publicUrl&quot;:&quot;localhost:8080&quot;}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其次，要存储文件内容，请发送 HTTP 多部分 POST 请求以</font></font><code>url + '/' + fid</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取响应：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; curl -F file=@/home/chris/myphoto.jpg http://127.0.0.1:8080/3,01637037d6
{"name":"myphoto.jpg","size":43234,"eTag":"1cc0118e"}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> curl -F file=@/home/chris/myphoto.jpg http://127.0.0.1:8080/3,01637037d6
{&quot;name&quot;:&quot;myphoto.jpg&quot;,&quot;size&quot;:43234,&quot;eTag&quot;:&quot;1cc0118e&quot;}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要更新，请发送另一个包含更新的文件内容的 POST 请求。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要删除，请向同一 URL 发送 HTTP DELETE 请求</font></font><code>url + '/' + fid</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; curl -X DELETE http://127.0.0.1:8080/3,01637037d6
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> curl -X DELETE http://127.0.0.1:8080/3,01637037d6" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保存文件 ID</font></font></h3><a id="user-content-save-file-id" class="anchor" aria-label="永久链接：保存文件 ID" href="#save-file-id"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在，您可以将</font></font><code>fid</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本例中的 3,01637037d6 保存到数据库字段中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开头的数字 3 代表卷 ID。逗号后面是一个文件密钥 01 和一个文件 cookie 637037d6。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">卷 ID 是一个无符号 32 位整数。文件密钥是一个无符号 64 位整数。文件cookie是一个无符号的32位整数，用于防止URL猜测。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件密钥和文件 cookie 均以十六进制编码。您可以以您自己的格式存储 &lt;volume id, file key, file cookie&gt; 元组，或者简单地将其存储</font></font><code>fid</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为字符串。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果存储为字符串，理论上需要 8+1+16+8=33 个字节。一个 char(33) 就足够了，因为大多数用途不需要 2^32 卷。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果空间确实是一个问题，您可以以您自己的格式存储文件 ID。您需要一个 4 字节整数作为卷 id，一个 8 字节长数字作为文件密钥，以及一个 4 字节整数作为文件 cookie。所以16个字节就足够了。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">读取文件</font></font></h3><a id="user-content-read-file" class="anchor" aria-label="永久链接：读取文件" href="#read-file"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是如何呈现 URL 的示例。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">首先通过文件的volumeId查找卷服务器的URL：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>&gt; curl http://localhost:9333/dir/lookup?volumeId=3
{"volumeId":"3","locations":[{"publicUrl":"localhost:8080","url":"localhost:8080"}]}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="> curl http://localhost:9333/dir/lookup?volumeId=3
{&quot;volumeId&quot;:&quot;3&quot;,&quot;locations&quot;:[{&quot;publicUrl&quot;:&quot;localhost:8080&quot;,&quot;url&quot;:&quot;localhost:8080&quot;}]}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由于（通常）没有太多卷服务器，并且卷不经常移动，因此您可以在大多数情况下缓存结果。根据复制类型，一个卷可以有多个副本位置。只需随机选择一个位置进行阅读即可。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在您可以获取公共 URL、渲染 URL 或通过 URL 直接从卷服务器读取：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code> http://localhost:8080/3,01637037d6.jpg
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value=" http://localhost:8080/3,01637037d6.jpg" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请注意，我们在此处添加了文件扩展名“.jpg”。它是可选的，并且只是客户端指定文件内容类型的一种方式。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您想要更好的 URL，可以使用以下替代 URL 格式之一：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code> http://localhost:8080/3/01637037d6/my_preferred_name.jpg
 http://localhost:8080/3/01637037d6.jpg
 http://localhost:8080/3,01637037d6.jpg
 http://localhost:8080/3/01637037d6
 http://localhost:8080/3,01637037d6
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value=" http://localhost:8080/3/01637037d6/my_preferred_name.jpg
 http://localhost:8080/3/01637037d6.jpg
 http://localhost:8080/3,01637037d6.jpg
 http://localhost:8080/3/01637037d6
 http://localhost:8080/3,01637037d6" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果你想获得图像的缩放版本，你可以添加一些参数：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200
http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200&amp;mode=fit
http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200&amp;mode=fill
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200
http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200&amp;mode=fit
http://localhost:8080/3/01637037d6.jpg?height=200&amp;width=200&amp;mode=fill" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机架感知和数据中心感知复制</font></font></h3><a id="user-content-rack-aware-and-data-center-aware-replication" class="anchor" aria-label="永久链接：机架感知和数据中心感知复制" href="#rack-aware-and-data-center-aware-replication"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 在卷级别应用复制策略。因此，当您获取文件 ID 时，您可以指定复制策略。例如：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>curl http://localhost:9333/dir/assign?replication=001
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="curl http://localhost:9333/dir/assign?replication=001" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复制参数选项有：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>000: no replication
001: replicate once on the same rack
010: replicate once on a different rack, but same data center
100: replicate once on a different data center
200: replicate twice on two different data center
110: replicate once on a different rack, and once on a different data center
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="000: no replication
001: replicate once on the same rack
010: replicate once on a different rack, but same data center
100: replicate once on a different data center
200: replicate twice on two different data center
110: replicate once on a different rack, and once on a different data center" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://github.com/seaweedfs/seaweedfs/wiki/Replication"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关复制的更多详细信息可以在 wiki 上</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以在启动主服务器时设置默认复制策略。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在特定数据中心分配文件密钥</font></font></h3><a id="user-content-allocate-file-key-on-specific-data-center" class="anchor" aria-label="永久链接：在特定数据中心分配文件密钥" href="#allocate-file-key-on-specific-data-center"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">卷服务器可以使用特定的数据中心名称启动：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code> weed volume -dir=/tmp/1 -port=8080 -dataCenter=dc1
 weed volume -dir=/tmp/2 -port=8081 -dataCenter=dc2
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value=" weed volume -dir=/tmp/1 -port=8080 -dataCenter=dc1
 weed volume -dir=/tmp/2 -port=8081 -dataCenter=dc2" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当请求文件密钥时，可选的“dataCenter”参数可以将分配的卷限制到特定的数据中心。例如，这指定分配的卷应限制为“dc1”：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code> http://localhost:9333/dir/assign?dataCenter=dc1
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value=" http://localhost:9333/dir/assign?dataCenter=dc1" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">其他特性</font></font></h3><a id="user-content-other-features" class="anchor" aria-label="永久链接：其他功能" href="#other-features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Failover-Master-Server"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无单点故障</font></font></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Optimization#insert-with-your-own-keys"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用您自己的钥匙插入</font></font></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Optimization#upload-large-files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对大文件进行分块</font></font></a></li>
<li><a href="https://github.com/seaweedfs/seaweedfs/wiki/Optimization#collection-as-a-simple-name-space"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">作为简单名称空间的集合</font></font></a></li>
</ul>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对象存储架构</font></font></h2><a id="user-content-object-store-architecture" class="anchor" aria-label="永久链接：对象存储架构" href="#object-store-architecture"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通常分布式文件系统将每个文件分割成块，中央主机保存文件名、块索引到块句柄的映射，以及每个块服务器拥有哪些块。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主要缺点是中央主控无法有效地处理许多小文件，并且由于所有读取请求都需要经过块主控，因此对于许多并发用户来说它可能无法很好地扩展。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 不管理块，而是管理主服务器中的数据卷。每个数据卷大小为32GB，可以容纳大量文件。并且每个存储节点可以有很多数据卷。所以主节点只需要存储有关卷的元数据，这是一个相当少量的数据，并且通常是稳定的。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实际的文件元数据存储在卷服务器上的每个卷中。由于每个卷服务器只管理自己磁盘上的文件元数据，每个文件只有16字节，因此所有文件访问都可以直接从内存中读取文件元数据，只需要一次磁盘操作即可真正读取文件数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了进行比较，请考虑 Linux 中的 xfs inode 结构为 536 字节。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主服务器和卷服务器</font></font></h3><a id="user-content-master-server-and-volume-server" class="anchor" aria-label="永久链接：主服务器和卷服务器" href="#master-server-and-volume-server"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该架构相当简单。实际数据存储在存储节点上的卷中。一台卷服务器可以有多个卷，并且可以通过基本身份验证支持读取和写入访问。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有卷均由主服务器管理。主服务器包含卷 ID 到卷服务器的映射。这是相当静态的信息，可以轻松缓存。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于每个写入请求，主服务器还会生成一个文件密钥，它是一个不断增长的 64 位无符号整数。由于写请求通常不像读请求那么频繁，因此一台主服务器应该能够很好地处理并发性。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">写入和读取文件</font></font></h3><a id="user-content-write-and-read-files" class="anchor" aria-label="永久链接：写入和读取文件" href="#write-and-read-files"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当客户端发送写请求时，主服务器返回该文件的（卷 ID、文件密钥、文件 cookie、卷节点 URL）。然后，客户端联系卷节点并 POST 文件内容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当客户端需要基于（卷 id、文件密钥、文件 cookie）读取文件时，它会通过卷 id 向主服务器询问（卷节点 URL、卷节点公共 URL），或者从缓存中检索该文件。然后客户端可以获取内容，或者只是在网页上呈现 URL，让浏览器获取内容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关写入-读取过程的详细信息，请参阅示例。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储大小</font></font></h3><a id="user-content-storage-size" class="anchor" aria-label="永久链接：存储大小" href="#storage-size"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在当前实现中，每个卷可以容纳 32 GB（32GiB 或 8x2^32 字节）。这是因为我们将内容对齐到 8 字节。通过更改 2 行代码，我们可以轻松地将其增加到 64GiB、128GiB 或更多，但代价是由于对齐而浪费了一些填充空间。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可以有 4 GB（4GiB 或 2^32 字节）的卷。因此系统总大小为 8 x 4GiB x 4GiB，即 128 exbibytes（128EiB 或 2^67 字节）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个单独的文件大小都受到卷大小的限制。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">节省内存</font></font></h3><a id="user-content-saving-memory" class="anchor" aria-label="永久链接：节省内存" href="#saving-memory"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">存储在卷服务器上的所有文件元信息都可以从内存中读取，无需访问磁盘。每个文件仅采用 &lt;64 位密钥、32 位偏移量、32 位大小&gt; 的 16 字节映射条目。当然，每个地图条目都有自己的地图空间成本。但通常磁盘空间先于内存耗尽。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分层存储到云端</font></font></h3><a id="user-content-tiered-storage-to-the-cloud" class="anchor" aria-label="永久链接：云的分层存储" href="#tiered-storage-to-the-cloud"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本地卷服务器速度要快得多，而云存储具有弹性容量，如果不经常访问（通常免费上传，但访问成本相对较高），实际上更具成本效益。凭借仅附加结构和 O(1) 访问时间，SeaweedFS 可以通过将热数据卸载到云端来利用本地和云存储。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通常热数据是新鲜的，温数据是旧的。 SeaweedFS 将新创建的卷放在本地服务器上，并可选择将旧卷上传到云上。如果较旧的数据访问频率较低，这实际上可以通过有限的本地服务器为您提供无限的容量，并且对于新数据来说仍然很快。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问时间为 O(1)，网络延迟成本保持在最低水平。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果热/温数据按照20/80分割，使用20台服务器，可以实现100台服务器的存储容量。这相当于节省了 80% 的成本！或者，您也可以重新调整 80 台服务器的用途来存储新数据，并获得 5 倍的存储吞吐量。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与其他文件系统相比</font></font></h2><a id="user-content-compared-to-other-file-systems" class="anchor" aria-label="永久链接：与其他文件系统相比" href="#compared-to-other-file-systems"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大多数其他分布式文件系统似乎比必要的更复杂。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 的设置和操作都旨在快速且简单。如果你到达这里时还不明白它是如何工作的，那么我们就失败了！请提出任何问题或更新此文件并进行澄清。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 不断前进。与其他系统相同。这些比较可能很快就会过时。请帮助保持更新。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与HDFS相比</font></font></h3><a id="user-content-compared-to-hdfs" class="anchor" aria-label="永久链接：与 HDFS 相比" href="#compared-to-hdfs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HDFS对每个文件使用块方法，非常适合存储大文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 非常适合快速并发地处理相对较小的文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 还可以通过将超大文件分割成可管理的数据块来存储超大文件，并将数据块的文件 ID 存储到元块中。这是由“杂草上传/下载”工具管理的，杂草主控或卷服务器对此不可知。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 GlusterFS、Ceph 相比</font></font></h3><a id="user-content-compared-to-glusterfs-ceph" class="anchor" aria-label="永久链接：与 GlusterFS、Ceph 相比" href="#compared-to-glusterfs-ceph"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些架构大部分是相同的。 SeaweedFS 旨在通过简单、扁平的架构快速存储和读取文件。主要区别是</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 针对小文件进行了优化，确保 O(1) 磁盘寻道操作，并且还可以处理大文件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 为文件静态分配卷 ID。定位文件内容只需查找卷 ID，即可轻松缓存。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS Filer元数据存储可以是任何众所周知且经过验证的数据存储，例如Redis、Cassandra、HBase、Mongodb、Elastic Search、MySql、Postgres、Sqlite、MemSql、TiDB、CockroachDB、Etcd、YDB等，并且易于定制。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS Volume服务器还通过HTTP直接与客户端通信，支持范围查询、直接上传等。</font></font></li>
</ul>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件元数据</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件内容读取</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">POSIX</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">休息API</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">针对大量小文件进行了优化</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">海藻FS</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查找卷 ID，可缓存</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">O(1) 磁盘寻道</font></font></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 文件管理器</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可线性扩展、可定制</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">O(1) 磁盘寻道</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保险丝</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GlusterFS</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">散列</font></font></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保险丝、NFS</font></font></td>
<td></td>
<td></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">头孢</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哈希+规则</font></font></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保险丝</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
<td></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">穆斯FS</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在记忆中</font></font></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保险丝</font></font></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最小IO</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个文件都有单独的元文件</font></font></td>
<td></td>
<td></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 GlusterFS 相比</font></font></h3><a id="user-content-compared-to-glusterfs" class="anchor" aria-label="永久链接：与 GlusterFS 相比" href="#compared-to-glusterfs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GlusterFS 将文件（包括目录和内容）存储在称为“bricks”的可配置卷中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GlusterFS 将路径和文件名散列到 ids 中，并分配给虚拟卷，然后映射到“bricks”。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 MooseFS 相比</font></font></h3><a id="user-content-compared-to-moosefs" class="anchor" aria-label="永久链接：与 MooseFS 相比" href="#compared-to-moosefs"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MooseFS选择忽略小文件问题。根据 moosefs 3.0 手册，“即使是一个小文件也会占用 64KiB 加上另外 4KiB 的校验和和 1KiB 的标头”，因为它“最初是为保存大量（如数千个）非常大的文件而设计的”</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MooseFS 主服务器将所有元数据保存在内存中。与 HDFS namenode 相同的问题。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 Ceph 相比</font></font></h3><a id="user-content-compared-to-ceph" class="anchor" aria-label="永久链接：与 Ceph 相比" href="#compared-to-ceph"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ceph 可以像 SeaweedFS 一样设置为 key-&gt;blob 存储。它要复杂得多，需要在其之上支持层。</font></font><a href="https://github.com/seaweedfs/seaweedfs/issues/120" data-hovercard-type="issue" data-hovercard-url="/seaweedfs/seaweedfs/issues/120/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是更详细的比较</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 有一个集中的主组来查找空闲卷，而 Ceph 使用哈希和元数据服务器来定位其对象。拥有集中式主机可以轻松编码和管理。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ceph 与 SeaweedFS 一样，基于对象存储 RADOS。 Ceph 相当复杂，评论褒贬不一。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ceph使用CRUSH哈希来自动管理数据放置，可以高效地定位数据。但数据必须按照CRUSH算法来放置。任何错误的配置都会导致数据丢失。拓扑变化，例如添加新服务器以增加容量，将导致高IO成本的数据迁移以适应CRUSH算法。 SeaweedFS 通过将数据分配给任何可写卷来放置数据。如果写入一个卷失败，只需选择另一个卷进行写入。添加更多卷也非常简单。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS 针对小文件进行了优化。小文件存储为一个连续的内容块，文件之间最多有 8 个未使用的字节。小文件访问是 O(1) 磁盘读取。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SeaweedFS Filer 使用现成的存储来管理文件目录，例如 MySql、Postgres、Sqlite、Mongodb、Redis、Elastic Search、Cassandra、HBase、MemSql、TiDB、CockroachCB、Etcd、YDB。这些商店经过验证、可扩展且更易于管理。</font></font></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">海藻FS</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 Ceph 相当</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">优势</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">掌握</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MDS</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更简单</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">体积</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">屏幕显示</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">针对小文件进行了优化</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">锉刀</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">头孢FS</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">线性可扩展、可定制、O(1) 或 O(logN)</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 MinIO 相比</font></font></h3><a id="user-content-compared-to-minio" class="anchor" aria-label="永久链接：与 MinIO 相比" href="#compared-to-minio"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO 紧密遵循 AWS S3，非常适合测试 S3 API。它具有良好的 UI、策略、版本控制等。SeaweedFS 正在努力迎头赶上。也可以稍后将MinIO作为网关放在SeaweedFS前面。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO 元数据位于简单文件中。每个文件写入都会对相应的元文件产生额外的写入。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO 没有针对大量小文件进行优化。文件只是按原样存储到本地磁盘。再加上用于纠删码的额外元文件和分片，只会放大 LOSF 问题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO有多个磁盘IO来读取一个文件。 SeaweedFS 的磁盘读取速度为 O(1)，即使对于纠删码文件也是如此。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO 具有全时擦除编码。 SeaweedFS 对热数据使用复制以提高速度，并可选择对热数据应用纠删码。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO 没有类似 POSIX 的 API 支持。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MinIO对存储布局有特定的要求。容量调整不灵活。在 SeaweedFS 中，只需启动一台指向主服务器的卷服务器。就这样。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发计划</font></font></h2><a id="user-content-dev-plan" class="anchor" aria-label="永久链接：开发计划" href="#dev-plan"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关如何管理和扩展系统的更多工具和文档。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">读取和写入流数据。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持结构化数据。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是一个超级令人兴奋的项目！我们需要帮助和</font></font><a href="https://www.patreon.com/seaweedfs" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">！</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装指南</font></font></h2><a id="user-content-installation-guide" class="anchor" aria-label="永久链接：安装指南" href="#installation-guide"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不熟悉golang的用户安装指南</font></font></p>
</blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1：在您的计算机上安装 go 并按照以下说明设置环境：</font></font></p>
<p dir="auto"><a href="https://golang.org/doc/install" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://golang.org/doc/install</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保定义你的 $GOPATH</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 步：签出此存储库：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git clone https://github.com/seaweedfs/seaweedfs.git</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git clone https://github.com/seaweedfs/seaweedfs.git" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第三步：执行以下命令下载、编译并安装项目</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c1">cd</span> seaweedfs/weed <span class="pl-k">&amp;&amp;</span> make install</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="cd seaweedfs/weed &amp;&amp; make install" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完成此操作后，您将在</font></font><code>$GOPATH/bin</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录中找到可执行文件“weed”</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">磁盘相关主题</font></font></h2><a id="user-content-disk-related-topics" class="anchor" aria-label="永久链接：磁盘相关主题" href="#disk-related-topics"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">硬盘性能</font></font></h3><a id="user-content-hard-drive-performance" class="anchor" aria-label="永久链接：硬盘性能" href="#hard-drive-performance"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在SeaweedFS上测试读取性能时，它基本上变成了对硬盘随机读取速度的性能测试。硬盘通常可达 100MB/s~200MB/s。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">固态硬盘</font></font></h3><a id="user-content-solid-state-disk" class="anchor" aria-label="永久链接：固态硬盘" href="#solid-state-disk"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要修改或删除小文件，SSD 必须一次删除整个块，并将现有块中的内容移动到新块。 SSD 在全新时速度很快，但随着时间的推移会变得碎片化，你必须进行垃圾收集、压缩块。 SeaweedFS 对 SSD 很友好，因为它是仅追加的。删除和压缩是在后台按音量级别完成的，不会减慢读取速度，也不会造成碎片。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准</font></font></h2><a id="user-content-benchmark" class="anchor" aria-label="永久链接：基准" href="#benchmark"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我自己在带固态盘的Mac Book上得到的不科学的单机结果，CPU：1个Intel Core i7 2.6GHz。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">写入100万个1KB文件：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>Concurrency Level:      16
Time taken for tests:   66.753 seconds
Completed requests:      1048576
Failed requests:        0
Total transferred:      1106789009 bytes
Requests per second:    15708.23 [#/sec]
Transfer rate:          16191.69 [Kbytes/sec]

Connection Times (ms)
              min      avg        max      std
Total:        0.3      1.0       84.3      0.9

Percentage of the requests served within a certain time (ms)
   50%      0.8 ms
   66%      1.0 ms
   75%      1.1 ms
   80%      1.2 ms
   90%      1.4 ms
   95%      1.7 ms
   98%      2.1 ms
   99%      2.6 ms
  100%     84.3 ms
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="Concurrency Level:      16
Time taken for tests:   66.753 seconds
Completed requests:      1048576
Failed requests:        0
Total transferred:      1106789009 bytes
Requests per second:    15708.23 [#/sec]
Transfer rate:          16191.69 [Kbytes/sec]

Connection Times (ms)
              min      avg        max      std
Total:        0.3      1.0       84.3      0.9

Percentage of the requests served within a certain time (ms)
   50%      0.8 ms
   66%      1.0 ms
   75%      1.1 ms
   80%      1.2 ms
   90%      1.4 ms
   95%      1.7 ms
   98%      2.1 ms
   99%      2.6 ms
  100%     84.3 ms" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随机读取100万个文件：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>Concurrency Level:      16
Time taken for tests:   22.301 seconds
Completed requests:      1048576
Failed requests:        0
Total transferred:      1106812873 bytes
Requests per second:    47019.38 [#/sec]
Transfer rate:          48467.57 [Kbytes/sec]

Connection Times (ms)
              min      avg        max      std
Total:        0.0      0.3       54.1      0.2

Percentage of the requests served within a certain time (ms)
   50%      0.3 ms
   90%      0.4 ms
   98%      0.6 ms
   99%      0.7 ms
  100%     54.1 ms
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="Concurrency Level:      16
Time taken for tests:   22.301 seconds
Completed requests:      1048576
Failed requests:        0
Total transferred:      1106812873 bytes
Requests per second:    47019.38 [#/sec]
Transfer rate:          48467.57 [Kbytes/sec]

Connection Times (ms)
              min      avg        max      std
Total:        0.0      0.3       54.1      0.2

Percentage of the requests served within a certain time (ms)
   50%      0.3 ms
   90%      0.4 ms
   98%      0.6 ms
   99%      0.7 ms
  100%     54.1 ms" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行 WARP 并启动混合基准测试。</font></font></h3><a id="user-content-run-warp-and-launch-a-mixed-benchmark" class="anchor" aria-label="永久链接：运行 WARP 并启动混合基准测试。" href="#run-warp-and-launch-a-mixed-benchmark"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>make benchmark
warp: Benchmark data written to "warp-mixed-2023-10-16[102354]-l70a.csv.zst"                                                                                                                                                                                               
Mixed operations.
Operation: DELETE, 10%, Concurrency: 20, Ran 4m59s.
 * Throughput: 6.19 obj/s

Operation: GET, 45%, Concurrency: 20, Ran 5m0s.
 * Throughput: 279.85 MiB/s, 27.99 obj/s

Operation: PUT, 15%, Concurrency: 20, Ran 5m0s.
 * Throughput: 89.86 MiB/s, 8.99 obj/s

Operation: STAT, 30%, Concurrency: 20, Ran 5m0s.
 * Throughput: 18.63 obj/s

Cluster Total: 369.74 MiB/s, 61.79 obj/s, 0 errors over 5m0s.
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="make benchmark
warp: Benchmark data written to &quot;warp-mixed-2023-10-16[102354]-l70a.csv.zst&quot;                                                                                                                                                                                               
Mixed operations.
Operation: DELETE, 10%, Concurrency: 20, Ran 4m59s.
 * Throughput: 6.19 obj/s

Operation: GET, 45%, Concurrency: 20, Ran 5m0s.
 * Throughput: 279.85 MiB/s, 27.99 obj/s

Operation: PUT, 15%, Concurrency: 20, Ran 5m0s.
 * Throughput: 89.86 MiB/s, 8.99 obj/s

Operation: STAT, 30%, Concurrency: 20, Ran 5m0s.
 * Throughput: 18.63 obj/s

Cluster Total: 369.74 MiB/s, 61.79 obj/s, 0 errors over 5m0s." tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要查看分段请求统计信息，请使用 --analyze.v 参数。</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>warp analyze --analyze.v warp-mixed-2023-10-16[102354]-l70a.csv.zst
18642 operations loaded... Done!
Mixed operations.
----------------------------------------
Operation: DELETE - total: 1854, 10.0%, Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.115 +0500 +05
 * Throughput: 6.19 obj/s

Requests considered: 1855:
 * Avg: 104ms, 50%: 30ms, 90%: 207ms, 99%: 1.355s, Fastest: 1ms, Slowest: 4.613s, StdDev: 320ms

----------------------------------------
Operation: GET - total: 8388, 45.3%, Size: 10485760 bytes. Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.12 +0500 +05
 * Throughput: 279.77 MiB/s, 27.98 obj/s

Requests considered: 8389:
 * Avg: 221ms, 50%: 106ms, 90%: 492ms, 99%: 1.739s, Fastest: 8ms, Slowest: 8.633s, StdDev: 383ms
 * TTFB: Avg: 81ms, Best: 2ms, 25th: 24ms, Median: 39ms, 75th: 65ms, 90th: 171ms, 99th: 669ms, Worst: 4.783s StdDev: 163ms
 * First Access: Avg: 240ms, 50%: 105ms, 90%: 511ms, 99%: 2.08s, Fastest: 12ms, Slowest: 8.633s, StdDev: 480ms
 * First Access TTFB: Avg: 88ms, Best: 2ms, 25th: 24ms, Median: 38ms, 75th: 64ms, 90th: 179ms, 99th: 919ms, Worst: 4.783s StdDev: 199ms
 * Last Access: Avg: 219ms, 50%: 106ms, 90%: 463ms, 99%: 1.782s, Fastest: 9ms, Slowest: 8.633s, StdDev: 416ms
 * Last Access TTFB: Avg: 81ms, Best: 2ms, 25th: 24ms, Median: 39ms, 75th: 65ms, 90th: 161ms, 99th: 657ms, Worst: 4.783s StdDev: 176ms

----------------------------------------
Operation: PUT - total: 2688, 14.5%, Size: 10485760 bytes. Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.115 +0500 +05
 * Throughput: 89.83 MiB/s, 8.98 obj/s

Requests considered: 2689:
 * Avg: 1.165s, 50%: 878ms, 90%: 2.015s, 99%: 5.74s, Fastest: 99ms, Slowest: 8.264s, StdDev: 968ms

----------------------------------------
Operation: STAT - total: 5586, 30.2%, Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.113 +0500 +05
 * Throughput: 18.63 obj/s

Requests considered: 5587:
 * Avg: 15ms, 50%: 11ms, 90%: 34ms, 99%: 80ms, Fastest: 0s, Slowest: 245ms, StdDev: 17ms
 * First Access: Avg: 14ms, 50%: 10ms, 90%: 33ms, 99%: 69ms, Fastest: 0s, Slowest: 203ms, StdDev: 16ms
 * Last Access: Avg: 15ms, 50%: 11ms, 90%: 34ms, 99%: 74ms, Fastest: 0s, Slowest: 203ms, StdDev: 17ms

Cluster Total: 369.64 MiB/s, 61.77 obj/s, 0 errors over 5m0s.
Total Errors:0.
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="warp analyze --analyze.v warp-mixed-2023-10-16[102354]-l70a.csv.zst
18642 operations loaded... Done!
Mixed operations.
----------------------------------------
Operation: DELETE - total: 1854, 10.0%, Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.115 +0500 +05
 * Throughput: 6.19 obj/s

Requests considered: 1855:
 * Avg: 104ms, 50%: 30ms, 90%: 207ms, 99%: 1.355s, Fastest: 1ms, Slowest: 4.613s, StdDev: 320ms

----------------------------------------
Operation: GET - total: 8388, 45.3%, Size: 10485760 bytes. Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.12 +0500 +05
 * Throughput: 279.77 MiB/s, 27.98 obj/s

Requests considered: 8389:
 * Avg: 221ms, 50%: 106ms, 90%: 492ms, 99%: 1.739s, Fastest: 8ms, Slowest: 8.633s, StdDev: 383ms
 * TTFB: Avg: 81ms, Best: 2ms, 25th: 24ms, Median: 39ms, 75th: 65ms, 90th: 171ms, 99th: 669ms, Worst: 4.783s StdDev: 163ms
 * First Access: Avg: 240ms, 50%: 105ms, 90%: 511ms, 99%: 2.08s, Fastest: 12ms, Slowest: 8.633s, StdDev: 480ms
 * First Access TTFB: Avg: 88ms, Best: 2ms, 25th: 24ms, Median: 38ms, 75th: 64ms, 90th: 179ms, 99th: 919ms, Worst: 4.783s StdDev: 199ms
 * Last Access: Avg: 219ms, 50%: 106ms, 90%: 463ms, 99%: 1.782s, Fastest: 9ms, Slowest: 8.633s, StdDev: 416ms
 * Last Access TTFB: Avg: 81ms, Best: 2ms, 25th: 24ms, Median: 39ms, 75th: 65ms, 90th: 161ms, 99th: 657ms, Worst: 4.783s StdDev: 176ms

----------------------------------------
Operation: PUT - total: 2688, 14.5%, Size: 10485760 bytes. Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.115 +0500 +05
 * Throughput: 89.83 MiB/s, 8.98 obj/s

Requests considered: 2689:
 * Avg: 1.165s, 50%: 878ms, 90%: 2.015s, 99%: 5.74s, Fastest: 99ms, Slowest: 8.264s, StdDev: 968ms

----------------------------------------
Operation: STAT - total: 5586, 30.2%, Concurrency: 20, Ran 5m0s, starting 2023-10-16 10:23:57.113 +0500 +05
 * Throughput: 18.63 obj/s

Requests considered: 5587:
 * Avg: 15ms, 50%: 11ms, 90%: 34ms, 99%: 80ms, Fastest: 0s, Slowest: 245ms, StdDev: 17ms
 * First Access: Avg: 14ms, 50%: 10ms, 90%: 33ms, 99%: 69ms, Fastest: 0s, Slowest: 203ms, StdDev: 16ms
 * Last Access: Avg: 15ms, 50%: 11ms, 90%: 34ms, 99%: 74ms, Fastest: 0s, Slowest: 203ms, StdDev: 17ms

Cluster Total: 369.64 MiB/s, 61.77 obj/s, 0 errors over 5m0s.
Total Errors:0." tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h2><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据 Apache 许可证 2.0 版（“许可证”）获得许可；除非遵守许可证，否则您不得使用此文件。您可以在以下位置获取许可证副本：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>http://www.apache.org/licenses/LICENSE-2.0
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="http://www.apache.org/licenses/LICENSE-2.0" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">除非适用法律要求或书面同意，否则根据许可证分发的软件均按“原样”分发，不带任何明示或暗示的保证或条件。请参阅许可证，了解许可证下管理权限和限制的特定语言。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本页的文本可根据 Creative Commons Attribution-Sharealike 3.0 Unported License 和 GNU Free Documentation License（无版本，没有不变部分、封面文本或封底文本）的条款进行修改和重复使用。</font></font></p>
<p dir="auto"><a href="#table-of-contents"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">返回目录</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随着时间的推移观星者</font></font></h2><a id="user-content-stargazers-over-time" class="anchor" aria-label="永久链接：随着时间的推移观星者" href="#stargazers-over-time"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a href="https://starchart.cc/chrislusf/seaweedfs" rel="nofollow"><img src="https://camo.githubusercontent.com/767fbd1e74e13b82b0455281a97d33d9cafe47b12f2604bae656ac4620d8f215/68747470733a2f2f7374617263686172742e63632f63687269736c7573662f7365617765656466732e737667" alt="随着时间的推移观星者" data-canonical-src="https://starchart.cc/chrislusf/seaweedfs.svg" style="max-width: 100%;"></a></p>
</article></div>
